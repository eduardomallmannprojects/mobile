import React from 'react';

import { View, YellowBox } from 'react-native';

YellowBox.ignoreWarnings(["Unrecognized WebSocket"])

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Routes from "./routes";

const App = () => <Routes />;

export default connect()(App);
